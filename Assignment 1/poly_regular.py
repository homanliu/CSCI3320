import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
from sklearn.linear_model import Ridge
from sklearn.preprocessing import PolynomialFeatures

X_train = [[5.3], [7.2], [10.5], [14.7], [18], [20]]
y_train = [[7.5], [9.1], [13.2], [17.5], [19.3], [19.5]]

X_test = [[6], [8], [11], [22]]
y_test = [[8.3], [12.5], [15.4], [19.6]]


# Polynomial regression on training data
poly = PolynomialFeatures(degree=5)
X_train_poly = poly.fit_transform(X_train)
X_test_poly = poly.transform(X_test)

# Create linear regression object
model = LinearRegression()

# Train the model using the training sets
model.fit(X_train_poly, y_train)
prediction = model.predict(X_test_poly)

score = model.score(X_test_poly, y_test)
print 'Linear regression (order 5) score is: '
print score
print ''

xx = np.linspace(0, 26, 100)
xx_poly = poly.transform(xx.reshape(xx.shape[0], 1))
yy_poly = model.predict(xx_poly)

# Linear regression (order 5) Visualization
plt.scatter(X_test, y_test, color='red')
plt.plot(xx, yy_poly, color='blue')
plt.title('Linear regression (order 5) result')
plt.xlabel('xx_plot')
plt.ylabel('yy_plot')
plt.show()


# Ridge Regression (with regularization)
ridge_model = Ridge(alpha=1, normalize=False)
ridge_model.fit(X_train_poly, y_train)

score = ridge_model.score(X_test_poly, y_test)
print 'Ridge regression (order 5) score is: '
print score

yy_ridge = ridge_model.predict(xx_poly)

# Ridge regression (order 5) Visualization
plt.scatter(X_test, y_test, color='red')
plt.plot(xx, yy_ridge, color='blue')
plt.title('Ridge regression (order 5) result')
plt.xlabel('xx_plot')
plt.ylabel('yy_plot')
plt.show()
