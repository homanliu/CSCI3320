#### CSCI 3320 Assignment 1

#### Liu Ho Man, 1155077343

##### 2.1.4	Linear regression on the preprocessed data

![linear_regression](Figure_1.png)

##### 2.2		Linear regression with multiple features

Parameter theta calculated by normal equation:
[[ 9.71445147e-17]
 [ 2.26006545e-01]
 [ 6.57546444e-01]
 [-1.45925621e-03]]

Parameter theta calculated by SGD:
[-1.84946358e-04  2.39492898e-01  6.37565587e-01 -7.64883329e-03]

##### 2.3.1	Polynomial regression on training data

y1 = 18.2339283 + (-6.05120366) $x$ + 1.11188554 $x^2$ + (-7.98839339e-02) $x^3$ + (2.73075958e-03) $x^4$ + (-3.75981662e-05) $x^5$

Linear regression (order 5) score is: 0.856746991702

![linear_regression](Figure_2.png)

##### 2.3.1	Ridge regression (with regularization)

y2 = 5.76871657 + (-2.93846420e-03) $x$ + (1.23508737e-02) $x^2$ + (1.43333276e-02) $x^3$ + (-1.10345880e-03) $x^4$ + (2.21101288e-05) $x^5$

Ridge regression (order 5) score is: 0.870668904577

![ridge_regression](Figure_3.png)

##### 2.3.3	Comparisons

1. Ridge regression (order 5) has the highest score.
2. The coefficient of $x^5$ has slightly increased when a larger $\alpha$ is applied.

##### 3.1		Binary Classification

The predictions of **X_test** contain values only 0 and 1 but not other.

![logistic_regression](Figure_4.png)

Number of wrong predictions is: 567