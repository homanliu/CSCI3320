import numpy as np
import matplotlib.pyplot as plt
from sklearn.datasets import make_blobs
from sklearn import linear_model, datasets
from sklearn.model_selection import train_test_split


n_samples = 10000

centers = [(-1, -1), (1, 1)]
X, y = make_blobs(n_samples=n_samples, n_features=2, cluster_std=1.8,
                  centers=centers, shuffle=False, random_state=42)

y[:n_samples // 2] = 0
y[n_samples // 2:] = 1

X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=42)
log_reg = linear_model.LogisticRegression()

log_reg.fit(X_train, y_train)
prediction = log_reg.predict(X_test)

class_1_x = []
class_1_y = []

class_0_x = []
class_0_y = []

for index, element in enumerate(prediction):
    if element == 0:
        class_0_x.append(X_test[index][0])
        class_0_y.append(X_test[index][1])
    else:
        class_1_x.append(X_test[index][0])
        class_1_y.append(X_test[index][1])

# Binary regression Visualization
plt.scatter(class_1_x, class_1_y, color='red', s=1)
plt.scatter(class_0_x, class_0_y, color='blue', s=1)
plt.title('Classification with Logistic Regression')
plt.xlabel('xx_plot')
plt.ylabel('yy_plot')
plt.show()

count = 0
for index, element in enumerate(prediction):
    if element != y_test[index]:
        count += 1
print 'Number of wrong predictions is: ' + str(count)