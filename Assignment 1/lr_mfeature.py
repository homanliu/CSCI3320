import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from sklearn import datasets, linear_model
import seaborn
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
seaborn.set()

# Read the raw data with pandas.read_csv()
df = pd.read_csv('imports-85.data',
            header=None,
            names=['symboling', 'normalized-losses', 'make', 'fuel-type', 'aspiration', 'num-of-doors', 'body-style',
                   'drive-wheels', 'engine-location', 'wheel-base', 'length', 'width', 'height', 'curb-weight',
                   'engine-type', 'num-of-cylinders', 'engine-size', 'fuel-system', 'bore', 'stroke', 'compression-ratio',
                   'horsepower', 'peak-rpm', 'city-mpg', 'highway-mpg', 'price'],
            na_values=('?'))

# Remove the data sample with missing value
df = df.dropna()
df_new = df[['horsepower','engine-size', 'peak-rpm']]

# Data standardization
X_scaler = StandardScaler()
trainX_scaled = X_scaler.fit_transform(df_new)

price_scaler = StandardScaler()
price_train_scaled = price_scaler.fit_transform(df['price'].values.reshape(-1, 1))


n, m = trainX_scaled.shape
X0 = np.ones((n,1))
X_new = np.hstack((X0, trainX_scaled))

X_transpose = np.transpose(X_new)
X_square_inverse = np.linalg.inv(np.dot(X_transpose, X_new))
theta = np.dot(np.dot(X_square_inverse, X_transpose), price_train_scaled)

print 'Parameter theta calculated by normal equation: '
print theta

model = linear_model.SGDRegressor(loss='squared_loss', alpha=0.01, max_iter=10000)
model.fit(trainX_scaled, price_train_scaled.ravel())

new_theta = np.hstack((model.intercept_, model.coef_))
print 'Parameter theta calculated by SGD: '
print new_theta