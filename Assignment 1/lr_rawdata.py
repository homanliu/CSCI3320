import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from sklearn import datasets, linear_model
import seaborn
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
seaborn.set()

# Read the raw data with pandas.read_csv()
df = pd.read_csv('imports-85.data',
            header=None,
            names=['symboling', 'normalized-losses', 'make', 'fuel-type', 'aspiration', 'num-of-doors', 'body-style',
                   'drive-wheels', 'engine-location', 'wheel-base', 'length', 'width', 'height', 'curb-weight',
                   'engine-type', 'num-of-cylinders', 'engine-size', 'fuel-system', 'bore', 'stroke', 'compression-ratio',
                   'horsepower', 'peak-rpm', 'city-mpg', 'highway-mpg', 'price'],
            na_values=('?'))

# Remove the data sample with missing value
df = df.dropna()

# Data standardization
test_set, train_set = train_test_split(df, train_size=0.2, test_size=0.8, shuffle=False)

horsepower_scaler = StandardScaler()
horsepower_train_scaled = horsepower_scaler.fit_transform(train_set['horsepower'].values.reshape(-1, 1))
horsepower_test_scaled = horsepower_scaler.transform(test_set['horsepower'].values.reshape(-1, 1))

price_scaler = StandardScaler()
price_train_scaled = price_scaler.fit_transform(train_set['price'].values.reshape(-1, 1))
price_test_scaled = price_scaler.transform(test_set['price'].values.reshape(-1, 1))

# Linear regression on the preprocessed data

# Create linear regression object
model = linear_model.LinearRegression()

# Train the model using the training sets
model.fit(horsepower_train_scaled, price_train_scaled)
prediction = model.predict(horsepower_test_scaled)

plt.scatter(horsepower_test_scaled, price_test_scaled, color='red')
plt.scatter(horsepower_test_scaled, prediction, color='blue')
plt.plot(horsepower_test_scaled, prediction, color='blue')
plt.title('Linear regression on cleaned and standardized test data')
plt.xlabel('Standardized horsepower')
plt.ylabel('Standardized price')
plt.show()