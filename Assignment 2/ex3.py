from __future__ import print_function
import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt

from sklearn.datasets import make_blobs
from sklearn.cluster import KMeans
from sklearn import metrics

def create_data():
    # Generate sample points
    centers = [[3,5], [5,1], [8,2], [6,8], [9,7]]
    X, y = make_blobs(n_samples=1000,centers=centers,cluster_std=0.5,random_state=3320)
    # =======================================
    # Complete the code here.
    # Plot the data points in a scatter plot.
    # Use color to represents the clusters.
    # =======================================
    plt.scatter(X[:,0], X[:,1], c=y)
    for centre in centers:
        plt.scatter(centre[0], centre[1], c='black')
    plt.show()
    return [X, y]

def my_clustering(X, y, n_clusters):
    # =======================================
    # Complete the code here.
    # return scores like this: return [score, score, score, score]
    # =======================================
    kmeans = KMeans(n_clusters=n_clusters, random_state=0).fit(X)
    labels = kmeans.labels_
    centres = kmeans.cluster_centers_
    plt.scatter(X[:,0], X[:,1], c=labels)
    for index, centre in enumerate(centres):
        plt.scatter(centre[0], centre[1], c='black')
        plt.annotate(index, centre)
        print ('Cluster ' + str(index + 1) + ' centre: ' + str(centre))
    plt.show()

    # negative values are bad (independent labelings), similar clusterings have a positive ARI.
    # 1.0 is the perfect match score.
    ARI = metrics.adjusted_rand_score(y, labels)

    # Values of exactly 0 indicate purely independent label assignments.
    # AMI of exactly 1 indicates that the two label assignments are equal.
    MRI = metrics.adjusted_mutual_info_score(y, labels)

    # score between 0.0 and 1.0. 1.0 stands for perfectly complete labeling
    V_measure = metrics.v_measure_score(y, labels)

    # The score is bounded between -1 for incorrect clustering and +1 for highly dense clustering.
    # Scores around zero indicate overlapping clusters.
    Silhouette = metrics.silhouette_score(X, labels)
    return [ARI, MRI, V_measure, Silhouette]

def main():
    X, y = create_data()
    range_n_clusters = [2, 3, 4, 5, 6]
    ari_score = [None] * len(range_n_clusters)
    mri_score = [None] * len(range_n_clusters)
    v_measure_score = [None] * len(range_n_clusters)
    silhouette_avg = [None] * len(range_n_clusters)

    for n_clusters in range_n_clusters:
        i = n_clusters - range_n_clusters[0]
        print("Number of clusters is: ", n_clusters)
        [ari_score[i], mri_score[i], v_measure_score[i], silhouette_avg[i]] = my_clustering(X, y, n_clusters)
        print('The ARI score is: ', ari_score[i])
        print('The MRI score is: ', mri_score[i])
        print('The v-measure score is: ', v_measure_score[i])
        print('The average silhouette score is: ', silhouette_avg[i])

    # =======================================
    # Complete the code here.
    # Plot scores of all four evaluation metrics as functions of n_clusters.
    # =======================================
    scores = [ari_score, mri_score, v_measure_score, silhouette_avg]
    titles = ['ARI score', 'MRI score', 'V-measure score', 'Silhouette score']
    for i in range(4):
        plt.subplot(1, 4, i + 1)
        plt.title(titles[i], fontsize=10)
        plt.plot(range_n_clusters, scores[i])
    plt.gray(), plt.show()

if __name__ == '__main__':
    main()

