import numpy as np
import scipy.io as sio
from sklearn.naive_bayes import MultinomialNB
from sklearn.metrics import accuracy_score

trainingData = sio.loadmat('train.mat')
print 'n_features in the dataset is: ' + str(trainingData['Xtrain'].shape[1])
print 'n_samples in the dataset is: ' + str(trainingData['ytrain'].shape[0])

trainingDataX = trainingData['Xtrain']
trainingDataY = trainingData['ytrain']

predictData = sio.loadmat('test.mat')

# Multinomial Naive Bayes classifier
GNBclassifier = MultinomialNB()
GNBclassifier.fit(trainingDataX, trainingDataY.ravel())
y_prediction = GNBclassifier.predict(trainingDataX)

print 'The accuracy of the Multinomial NB classifier: ' + str(accuracy_score(trainingDataY, y_prediction) * 100.0) + '%'
# ===============================

predictTestData = GNBclassifier.predict(predictData['Xtest'])

np.savetxt('prediction.txt', predictTestData, fmt='%d', newline = '\n')