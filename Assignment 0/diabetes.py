import matplotlib.pyplot as plt
import numpy as np
from sklearn import datasets, linear_model
import seaborn; seaborn.set()

# Load the diabetes dataset
diabetes = datasets.load_diabetes()

# which feature
i_feature = 0
# Get the feature name
feature_names = ['Age', 'Sex', 'Body mass index', 'Average blood pressure', 'S1',
                 'S2', 'S3', 'S4', 'S5', 'S6']

# Use only one feature
diabetes_X = diabetes.data[:, np.newaxis, i_feature]

# Split the data into training/testing sets
diabetes_X_train = diabetes_X[:-20]
diabetes_X_test = diabetes_X[-20:]

# Split the targets into training/testing sets
diabetes_y_train = diabetes.target[:-20]
diabetes_y_test = diabetes.target[-20:]

# Create linear regression object
model = linear_model.LinearRegression()

# Train the model using the training sets
model.fit(diabetes_X_train, diabetes_y_train)

# Explained variance score: score=1 is perfect prediction
model_score = model.score(diabetes_X_test, diabetes_y_test)
