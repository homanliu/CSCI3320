import matplotlib.pyplot as plt
import numpy as np
from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error
import seaborn; seaborn.set()
import operator

# Load the diabetes dataset
diabetes = datasets.load_diabetes()

print 'Number of features in the Diabetes dataset is: ' + str(diabetes['data'].shape[1])
print 'Number of samples in the Diabetes dataset is: ' + str(diabetes['data'].shape[0])

# Get the feature name
feature_names = ['Age', 'Sex', 'Body mass index', 'Average blood pressure', 'S1',
                'S2', 'S3', 'S4', 'S5', 'S6']
feature_scores = []

# which feature
for i_feature in range(len(diabetes['feature_names'])):

    # Use only one feature
    diabetes_X = diabetes.data[:, np.newaxis, i_feature]

    # Split the data into training/testing sets
    diabetes_X_train = diabetes_X[:-20]
    diabetes_X_test = diabetes_X[-20:]

    # Split the targets into training/testing sets
    diabetes_y_train = diabetes.target[:-20]
    diabetes_y_test = diabetes.target[-20:]

    # Create linear regression object
    model = linear_model.LinearRegression()

    # Train the model using the training sets
    model.fit(diabetes_X_train, diabetes_y_train)

    # Explained variance score: score=1 is perfect prediction
    model_score = model.score(diabetes_X_test, diabetes_y_test)
    feature_scores.append(model_score)

dicts = dict(zip(feature_names, feature_scores))
sortedFeature = sorted(dicts.items(), key=operator.itemgetter(1), reverse=True)
sortedFeatureName, sortedFeatureScore = zip(*sortedFeature)
print 'Ordered list of feature names is: {}'.format(sortedFeatureName)
print 'Ordered list of model scores is: {}'.format(sortedFeatureScore)

for index, score in enumerate(feature_scores):
    if score == sortedFeatureScore[0]:
        break

# Train the model for best fitted model
diabetes_X = diabetes.data[:, np.newaxis, index]
diabetes_X_train = diabetes_X[:-20]
diabetes_X_test = diabetes_X[-20:]
diabetes_y_train = diabetes.target[:-20]
diabetes_y_test = diabetes.target[-20:]
model = linear_model.LinearRegression()
model.fit(diabetes_X_train, diabetes_y_train)

prediction = model.predict(diabetes_X_test)
lostValue = mean_squared_error(diabetes_y_test, prediction)
print 'Value of the loss function for the best fitted model is: ' + str(lostValue)
# ==============================================

plt.scatter(diabetes_X_test, diabetes_y_test, color='red')
plt.scatter(diabetes_X_test, prediction, color='blue')
plt.plot(diabetes_X_test, prediction, color='blue')
plt.title('Visualization of predictions')
plt.xlabel(sortedFeatureName[0])
plt.ylabel('Disease Progression')
plt.show()