### CSCI 3320 Programming Assignment 0

Liu Ho Man, 1155077343

#### Part 3: Linear Regression in Scikit-learn

1. Get `n_features` and `n_samples`

   ```python
   Number of features in the Diabetes dataset is: 10
   Number of samples in the Diabetes dataset is: 442
   ```

2. Find out how each feature fits the disease progression

   ```python
   Ordered list of feature names is: ('Body mass index', 'S5', 'Average blood pressure', 'S3', 'S4', 'S6', 'Age', 'Sex', 'S2','S1')
   Ordered list of model scores is: (0.4725754479822712, 0.3948984231023219, 0.15995117339547205, 0.060610607792839555, -0.004070338973065413, -0.08990371992812851, -0.1327020163062087, -0.13883792137588857, -0.15171870558112976, -0.16094176987655562)
   ```

3. Calculate the loss function

   ```python
   Value of the loss function for the best fitted model is: 2548.0723987259707
   ```

4. ​

![figure 1](figure_1.png)

#### Part 4: Naive Bayes Classification in Scikit-learn

1. Load the `train.mat` data

```python
n_features in the dataset is: 1899
n_samples in the dataset is: 6665
```

2. Choose a Naive Bayes classifier

   * Gaussian NB: It is suitable for continuous features and it assumed the data is distributed according to normal distribution
   * Multinomial NB: It is suitable for discrete features and represents the frequencies of each features happened
   * Bernoulli NB: It is suitable for discrete features and it considers either true or false, the feature exists or not

   Therefore, Multinomial NB is suitable for this spam detection.

```python
The accuracy of the Multinomial NB classifier: 98.33458364591148%
```